'use strict';   // See note about 'use strict'; below

var myApp = angular.module('myApp', [
 'ngRoute',
 'forerunnerdb'
]);
myApp.config(['$routeProvider',
     function($routeProvider) {
         $routeProvider.
             when('/', {
                 templateUrl: 'static/partials/index.html',
             }).
             when('/about', {
                 templateUrl: 'static/partials/about.html',
             }).
             when('/dbTest', {
                 templateUrl: 'static/partials/dbTest.html',
             }).
             otherwise({
                 redirectTo: 'static/partials/index.html'
             });
     }
]);
/*
myApp.run(function ($rootScope, $fdb) {
        // Define a ForerunnerDB database on the root scope (optional)
        $rootScope.$db = $fdb.db('myDatabase');
});
*/
myApp.controller('mainController', ['$scope', '$fdb', function ($scope, $fdb) {
        $fdb
            .db('myDatabase')
            .collection('myCollection')
            .ng($scope, 'myData');
        $fdb.db('myDatabase').collection('myCollection').load(function() {
            $fdb.db('myDatabase').collection('myCollection').insert([{
                    _id: 12,
                    val: 1
                }, {
                    _id: 17,
                    val: 2
                }, {
                    _id: 18,
                    val: 3
            }]);
            $fdb.db('myDatabase').collection('myCollection').remove([{
                    _id: 18
            }]);


            $fdb.db('myDatabase').collection('myCollection').save(function (err) {
                if (!err) {
                    // Save was successful
                    console.log($fdb.db('myDatabase').collections());
                }
                else{alert("error, did not save");}
            });
        });



}]);
